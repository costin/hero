#HERO app

###1. How to install
You can run this app from my Vagrant test box.

In order to do so,
``git clone git@bitbucket.org:costin/testbox.git``. Then, from the testbox folder, run ```vagrant up```.
After the box is up, go to the ```projectfiles``` folder, then ```git clone git@bitbucket.org:costin/hero.git```.
 
At this point, you should log into the box, ```vagrant ssh```, go to the ```/vagrant/projectfiles/hero``` folder, and run ```composer install```.

###2. Running the app
The app can now be accessed by going to ```http://testbox.local/hero/``` in your browser.

###3. Running the tests
The tests can be run from the hero folder within the VM, entering this command: ```bin/phpunit --bootstrap vendor/autoload.php test/services/GameServiceTest```