<?php

use App\Entities\Players\Orderus;
use App\Entities\Players\Wildling;
use App\Framework\Container;
use App\Framework\Log;
use App\Framework\Probability;
use App\Services\GameService;
use PHPUnit\Framework\TestCase;

class GameServiceTest extends TestCase
{
    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Orderus
     */
    protected $orderus;

    /**
     * @var Wildling
     */
    protected $wildling;

    /**
     * @var GameService
     */
    protected $gameService;

    protected function setUp()
    {
        $this->container = new Container();
        $this->container
            ->add('log', Log::getInstance())
            ->add('probability', Probability::getInstance());

        $this->orderus = new Orderus();
        $this->wildling = new Wildling();
    }

    // Wildling and Orderus have same spped, but Orderus has higher luck: Orderus should be PlayerOne
    public function testSetPlayersOrder()
    {
        // given
        $this->wildling->speed->setValue(50);
        $this->orderus->speed->setValue(50);

        $this->wildling->luck->setValue(25);
        $this->orderus->luck->setValue(30);

        $this->gameService = GameService::getInstance($this->orderus, $this->wildling, $this->container);

        // when
        $this->gameService->setPlayersOrder($this->orderus, $this->wildling);

        // then
        $this->assertSame($this->gameService->getPlayerOne(), $this->orderus);
        $this->assertSame($this->gameService->getPlayerTwo(), $this->wildling);
    }

    // Wildling is maxed out, Ordesus in at its lowest, Wildling should always win
    public function testSimpleGame()
    {
        // given
        $this->wildling->speed->setValue(60);
        $this->orderus->speed->setValue(40);

        $this->wildling->health->setValue(90);
        $this->orderus->health->setValue(70);

        $this->wildling->strength->setValue(90);
        $this->orderus->defence->setValue(45);

        $this->gameService = GameService::getInstance($this->orderus, $this->wildling, $this->container);

        // when
        $winner = $this->gameService->play();

        // then
        $this->assertSame($winner, 'Wildling');
    }
}