<?php

namespace App\Framework;

class Container
{
    private $keys = [];

    public function add($id, $value)
    {
        $this->keys[$id] = $value;

        return $this;
    }

    public function get($id)
    {
        return $this->keys[$id];
    }
}
