<?php

namespace App\Framework;

class Log
{
    private $logEntries = [];

    public static function getInstance()
    {
        return new self();
    }

    public function log($entry)
    {
        $this->logEntries[] = $entry;

        return $this;
    }

    /**
     * @return array
     */
    public function getLogEntries()
    {
        return $this->logEntries;
    }
}
