<?php

namespace App\Framework;

class Probability
{
    public static function getInstance()
    {
        return new self();
    }

    public function calculateNewProbability()
    {
        return rand(1, 100);
    }
}
