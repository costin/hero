<?php

namespace App\Services;

use App\Entities\Players\Player;
use App\Framework\Container;
use App\Framework\Log;
use App\Framework\Probability;

class GameService
{
    /**
     * @var Player
     */
    private $playerOne;

    /**
     * @var Player
     */
    private $playerTwo;

    /**
     * @var Player
     */
    private $winner;

    /**
     * @var int
     */
    private $turn = 1;

    /**
     * @var Log
     */
    private $fightLog;

    /**
     * @var Probability
     */
    private $probability;

    public static function getInstance(Player $orderus, Player $wildling, Container $container)
    {
        return new self($orderus, $wildling, $container);
    }

    private function __construct(Player $orderus, Player $wildling, Container $container)
    {
        $this->setPlayersOrder($orderus, $wildling);

        $this->setFightLog($container->get('log'));
        $this->setProbability($container->get('probability'));
    }

    public function setPlayersOrder(Player $orderus, Player $wildling)
    {
        // decide which of the two players is playerOne - the first to start
        $playerOne = $orderus;
        $playerTwo = $wildling;

        if ($wildling->speed->getValue() > $orderus->speed->getValue()) {
            $playerOne = $wildling;
            $playerTwo = $orderus;
        }

        if ($wildling->speed->getValue() == $orderus->speed->getValue()) {
            if ($wildling->luck->getValue() > $orderus->luck->getValue()) {
                $playerOne = $wildling;
                $playerTwo = $orderus;
            }
        }

        $this->setPlayerOne($playerOne);
        $this->setPlayerTwo($playerTwo);
    }

    public function play()
    {
        $round = 0;
        while (!$this->isGameOver()) {
            $round++;

            $this->fightLog
                ->log('Round ' . $round)
                ->log($this->playerOne->name . ' health: ' . $this->playerOne->health->getValue())
                ->log($this->playerTwo->name . ' health: ' . $this->playerTwo->health->getValue());

            switch ($this->turn % 2) {
                // playerTwo's turn
                case 0:
                    $this->fightLog->log($this->playerTwo->name . ' strikes!');

                    $this->fight($this->playerTwo, $this->playerOne);
                    break;
                // playerOne's turn
                case 1:
                    $this->fightLog->log($this->playerOne->name . ' strikes!');

                    $this->fight($this->playerOne, $this->playerTwo);
                    break;
            }

            $this->turn++;
            $this->fightLog->log('');
        }

        if (!empty($this->winner)) {
            return $this->winner->name;
        } else {
            return 'Tie';
        }
    }

    private function isGameOver()
    {
        if ($this->turn >= 20) {
            return true;
        }

        if ($this->playerOne->health->getValue() <= 0) {
            $this->winner = $this->playerTwo;
            return true;

        }

        if ($this->playerTwo->health->getValue() <= 0) {
            $this->winner = $this->playerOne;
            return true;
        }

        return false;
    }

    private function fight(Player $attacker, Player $defender)
    {
        $damage = $attacker->strength->getValue() - $defender->defence->getValue();

        // apply attack skills
        foreach ($attacker->getAttackSkills() as $skill) {
            $probability = $this->probability->calculateNewProbability();

            if ($probability < $skill->getProbability()) {
                $this->fightLog->log($skill->name  . ' skill used!');

                $this->turn = $skill->alterTurn($this->turn);
            }
        }

        // apply defend skills
        foreach ($defender->getDefenceSkills() as $skill) {
            $probability = $this->probability->calculateNewProbability();

            if ($probability < $skill->getProbability()) {
                $this->fightLog->log($skill->name  . ' skill used!');

                $damage = $skill->alterDamage($damage);
            }
        }

        $probability = $this->probability->calculateNewProbability();
        if ($probability < $defender->luck->getValue()) {
            $this->fightLog->log('Luck struck! No damage.');

            $damage = 0;
        }

        $this->fightLog->log('Damage caused is ' . $damage);

        $defender->health->setValue($defender->health->getValue() - $damage);
    }

    /**
     * @param Player $playerOne
     */
    public function setPlayerOne($playerOne)
    {
        $this->playerOne = $playerOne;
    }

    /**
     * @param Player $playerTwo
     */
    public function setPlayerTwo($playerTwo)
    {
        $this->playerTwo = $playerTwo;
    }

    /**
     * @param Log $fightLog
     */
    public function setFightLog($fightLog)
    {
        $this->fightLog = $fightLog;
    }

    /**
     * @param Probability $probability
     */
    public function setProbability($probability)
    {
        $this->probability = $probability;
    }

    /**
     * @return Player
     */
    public function getPlayerOne()
    {
        return $this->playerOne;
    }

    /**
     * @return Player
     */
    public function getPlayerTwo()
    {
        return $this->playerTwo;
    }
}
