<?php

namespace App\Entities\Skills;

class Skill
{
    const DEFENCE_TYPE = 'DEFENCE';
    const ATTACK_TYPE = 'ATTACK';

    protected $type;

    protected $probability;

    public $name = 'Default skill';

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getProbability()
    {
        return $this->probability;
    }

    public function alterDamage($damage)
    {
        return $damage;
    }

    public function alterTurn($turn)
    {
        return $turn;
    }
}
