<?php

namespace App\Entities\Skills;

class RapidStrike extends Skill
{
    protected $type = Skill::ATTACK_TYPE;

    protected $probability = 10;

    public $name = 'RapidStrike';

    public function alterTurn($turn)
    {
        return $turn - 1;
    }
}
