<?php

namespace App\Entities\Skills;

class MagicShield extends Skill
{
    protected $type = Skill::DEFENCE_TYPE;

    protected $probability = 20;

    public $name = 'MagicShield';

    public function alterDamage($damage)
    {
        return $damage / 2;
    }
}
