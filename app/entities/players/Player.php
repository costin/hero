<?php

namespace App\Entities\Players;

use App\Entities\Skills\Skill;
use App\Entities\Stats\Defence;
use App\Entities\Stats\Health;
use App\Entities\Stats\Luck;
use App\Entities\Stats\Speed;
use App\Entities\Stats\Strength;

class Player
{
    /**
     * @var Health
     */
    public $health;

    /**
     * @var Strength
     */
    public $strength;

    /**
     * @var Defence
     */
    public $defence;

    /**
     * @var Speed
     */
    public $speed;

    /**
     * @var Luck
     */
    public $luck;

    public $name = 'Default Player';

    protected $minHealth;
    protected $maxHealth;

    protected $minStrength;
    protected $maxStrength;

    protected $minDefence;
    protected $maxDefence;

    protected $minSpeed;
    protected $maxSpeed;

    protected $minLuck;
    protected $maxLuck;

    /**
     * @var Skill[]
     */
    protected $skills = [];

    /**
     * @return array
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @return Skill[]
     */
    public function getDefenceSkills()
    {
        $returnedSkills = [];

        foreach ($this->skills as $skill) {
            if ($skill->getType() == Skill::DEFENCE_TYPE) {
                $returnedSkills[] = $skill;
            }
        }

        return $returnedSkills;
    }

    /**
     * @return Skill[]
     */
    public function getAttackSkills()
    {
        $returnedSkills = [];

        foreach ($this->skills as $skill) {
            if ($skill->getType() == Skill::ATTACK_TYPE) {
                $returnedSkills[] = $skill;
            }
        }

        return $returnedSkills;
    }

    /**
     * @param array $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @param $skill
     * @return Player
     */
    public function addSkill($skill)
    {
        $this->skills[] = $skill;

        return $this;
    }

    public function __construct()
    {
        $this->health = new Health($this->minHealth, $this->maxHealth);
        $this->strength = new Strength($this->minStrength, $this->maxStrength);
        $this->defence = new Defence($this->minDefence, $this->maxDefence);
        $this->speed = new Speed($this->minSpeed, $this->maxSpeed);
        $this->luck = new Luck($this->minLuck, $this->maxLuck);
    }
}
