<?php

namespace App\Entities\Players;

class Orderus extends Player
{
    public $name = 'Orderus';

    protected $minHealth = 70;
    protected $maxHealth = 100;

    protected $minStrength = 70;
    protected $maxStrength = 80;

    protected $minDefence = 45;
    protected $maxDefence = 55;

    protected $minSpeed = 40;
    protected $maxSpeed = 50;

    protected $minLuck = 10;
    protected $maxLuck = 30;
}
