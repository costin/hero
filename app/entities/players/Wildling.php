<?php

namespace App\Entities\Players;

class Wildling extends Player
{
    public $name = 'Wildling';

    protected $minHealth = 60;
    protected $maxHealth = 90;

    protected $minStrength = 60;
    protected $maxStrength = 90;

    protected $minDefence = 40;
    protected $maxDefence = 60;

    protected $minSpeed = 40;
    protected $maxSpeed = 60;

    protected $minLuck = 25;
    protected $maxLuck = 40;
}
