<?php

use App\Entities\Players\Orderus;
use App\Entities\Players\Wildling;
use App\Entities\Skills\MagicShield;
use App\Entities\Skills\RapidStrike;
use App\Framework\Container;
use App\Framework\Log;
use App\Framework\Probability;
use App\Services\GameService;

require_once dirname(__FILE__) . '/../vendor/autoload.php';

// initialize the two players
$orderus = new Orderus();
$wildling = new Wildling();

// attach Orderus' skills
$orderus
    ->addSkill(new MagicShield())
    ->addSkill(new RapidStrike());

$container = new Container();
$container
    ->add('log', Log::getInstance())
    ->add('probability', Probability::getInstance());

// initiate gameService
$gameService = GameService::getInstance($orderus, $wildling, $container);

// play!
$winner = $gameService->play();

echo 'The winner is: ' . $winner . '<br><br>';

echo(implode('<br>', $container->get('log')->getLogEntries()));